﻿using System;

namespace ex_22
{
    class Program
    {
        static void Main(string[] args)
        {
           var movies = new string[5] {"Deadpool", "Seven", "Burn", "catch me now", "hello"};

           Console.WriteLine(string.Join(", ", movies) );
        
           movies[0] = "wanted";
           movies[2] = "With Paris with love";

           Console.WriteLine(string.Join(", ", movies) );

           Array.Sort(movies);
           Console.WriteLine(string.Join(", ", movies) );

           Console.WriteLine(movies.Length);

           Console.WriteLine(string.Join(", ", movies) );

           var names = new List<string> {"sushi", "pizza", "burgers", "turkish", "milkshakes"};

           names.remove("burgers");

           Console.WriteLine(names);

        }
    }
}
